from conans import (
    ConanFile,
    tools,
    CMake,
)
import os


class ConanCmakeExample(ConanFile):
    name = "conan-cmake-example"
    version = "0.0.1"
    settings = "os", "arch", "compiler", "build_type",
    exports_sources = "src/*", "CMakeLists.txt", "conan/*",
    generators = "cmake",

    def build(self):
        cmake = CMake(self)
        conan_inject_script = os.path.join(
            self.source_folder, "conan", "inject_conan.cmake",
        )
        cmake.definitions.update(
            CONAN_INSTALL_FOLDER=self.install_folder,
            CMAKE_PROJECT_conan_cmake_example_INCLUDE=conan_inject_script,
        )
        cmake.configure(build_folder=os.path.join(self.build_folder, "build"))
        cmake.build()
        cmake.install()

    def package(self):
        pass
